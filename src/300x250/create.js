function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont(){
        add();
    }

    function add(){


        ___("h1")
            .text(dd.copy.h1_rect, {webfont:"open_sansbold", color:dd.style.h1Color,fontSize:15, width:200 })
            .style({css:"line-height:24px"})
            .position({top:65, left:20});

        ___("h2")
            .text(dd.copy.h2_rect, {webfont:"open_sansbold", color:dd.style.h2Color,fontSize:15, width:125})
            .style({css:"line-height:24px"})
            .position({top:65, left:20});


        ___("key_holder>key")
            .position({top:100, left:-50})
            .style({width:config.bannerWidth, height:config.bannerHeight, greensock:{alpha:0}});
        // ___("key>key2")
        //     .image(fasset("key1.png"), {width:215, height:217, fit:true})
        //     .position({centerX:0, centerY:0})

        // ___("key>key1")
        //     .image(fasset("key1.png"), {width:215, height:216, fit:true})
        //     .position({centerX:0, centerY:0});

        // ___("key>key1")
        //     .image(asset("key3.png"), {width:215, height:216, fit:true})
        //     .position({centerX:0, centerY:0});

        ___("key>eye_shadow")
            .style({width:220, height:220, greensock:{alpha:0}, css:"z-index:1; border-radius:100%; -webkit-box-shadow: inset -45px 44px 157px 0px rgba(41,61,14,1); -moz-box-shadow: inset -45px 44px 157px 0px rgba(41,61,14,1); box-shadow: inset -45px 44px 157px 0px rgba(41,61,14,1);"})
            .position({centerX:0, centerY:0});

        ___("key>eye")
            .image(asset("key3.png"), {width:215, height:214, fit:true})
            .position({centerX:0, centerY:0})
                    

        ___("key>eye_mid")
            .image(asset("eye_mid.png"), {width:63, height:63, fit:true})
            .position({centerX:2, centerY:2});
        
        ___("deloitte_logo")
            .image(asset("deloitte-logo.png"), {width:120, height:25, fit:true})
            .position({left:20, top:20});


        ___("cta")
            .text(dd.copy.cta, {webfont:"open_sansbold", color:dd.style.ctaColor, fontSize:11, width:65})
            .position({right:46, bottom:26})
            .style({css:"padding:5px 15px; text-align: center; background-color: #0AA0DB;"});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTAG);
        }
    }
}

module.exports.setElements = setElements;