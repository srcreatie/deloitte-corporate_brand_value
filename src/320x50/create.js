function setElements(callback) {

    config = {};
    config.bannerWidth = 320;
    config.bannerHeight = 50;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont(){
        add()
    }

    function add(){

        ___("h1")
            .text(dd.copy.h1_rect, {webfont:"open_sansbold", color:dd.style.h1Color,fontSize:11, width:250 })
            .style({css:"line-height:12px"})
            .position({top:6, left:95});

        ___("h2")
            .text(dd.copy.h2_leader, {webfont:"open_sansbold", color:dd.style.h2Color,fontSize:11, width:250})
            .style({css:"line-height:12px"})
            .position({top:6, left:95});

        ___("deloitte_logo")
            .image(asset("deloitte-logo.png"), {width:75, height:15, fit:true})
            .position({left:10, centerY:-1});

        ___("cta")
            .text(dd.copy.cta, {webfont:"open_sansbold", color:dd.style.ctaColor, fontSize:9, width:50})
            .position({right:32, bottom:21})
            .style({css:"padding:3px 12px; text-align: center; background-color: #0AA0DB;"});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTAG);
        }
    }
}

module.exports.setElements = setElements;