function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1_rect = "The GDPR requires <br />changes that we believe <br />bring opportunities!";
    devDynamicContent.srFeed[0].copy.h1_sky = "The GDPR <br />requires <br />changes <br />that we <br />believe bring <br />opportunities!";
    devDynamicContent.srFeed[0].copy.h2_rect= "Learn more <br />about this and<br />sign up for the<br /> email alerts.";
    devDynamicContent.srFeed[0].copy.h2_leader= "Learn more about <br />this and sign up for <br />the email alerts.";
    devDynamicContent.srFeed[0].copy.h2_sky= "Learn more <br />about this <br />and sign up<br /> for the email <br />alerts.";
    /*devDynamicContent.srFeed[0].copy.h1_rect = "The GDPR requires changes that we believe bring opportunities!";
    devDynamicContent.srFeed[0].copy.h2_rect= "Learn more about this and sign up for the email alerts.";*/
    devDynamicContent.srFeed[0].copy.cta = "More info";
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.h1Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.h2Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.ctaColor = "#FFFFFF";
    devDynamicContent.srFeed[0].background300x250 = dimension("example-300x250.png", "300x250");

    return devDynamicContent;
}

module.exports = setData;