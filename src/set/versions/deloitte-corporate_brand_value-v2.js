function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1_rect = "How can blockchain <br />technology help <br />your business?";
    devDynamicContent.srFeed[0].copy.h1_sky = "How can blockchain technology help your business?";
    devDynamicContent.srFeed[0].copy.h2_rect= "Discover and apply the advantages";
    devDynamicContent.srFeed[0].copy.h2_leader= "Discover and apply <br />the advantages";
    devDynamicContent.srFeed[0].copy.h2_sky= "Discover and apply the advantages";

    devDynamicContent.srFeed[0].copy.cta = "More info";
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.h1Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.h2Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.ctaColor = "#FFFFFF";    

    return devDynamicContent;
}

module.exports = setData;

