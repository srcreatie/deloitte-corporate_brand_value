function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};

    devDynamicContent.srFeed[0].copy.h1_rect = "Artificial Intelligence is on the rise.";

    devDynamicContent.srFeed[0].copy.h1_sky = "Artificial Intelligence is on the rise.";
    devDynamicContent.srFeed[0].copy.h2_rect= "But what can it do for your organization?";
    devDynamicContent.srFeed[0].copy.h2_leader= "But what can it do for<br />your organization?";
    devDynamicContent.srFeed[0].copy.h2_sky= "But what can it do for your organization?";
    
    devDynamicContent.srFeed[0].copy.cta = "More info";
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.h1Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.h2Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.ctaColor = "#FFFFFF";    

    return devDynamicContent;
}

module.exports = setData;
