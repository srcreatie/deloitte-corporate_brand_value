function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};

    devDynamicContent.srFeed[0].copy.h1_rect = "Ready to embrace AI<br />successfully in your<br />company?";

    devDynamicContent.srFeed[0].copy.h1_sky = "Ready to embrace AI successfully in your company?";
    devDynamicContent.srFeed[0].copy.h2_rect= "Deloitte’s Artificial Intelligence Center<br />of Expertise tells<br />you how";
    devDynamicContent.srFeed[0].copy.h2_leader= "Deloitte’s Artificial<br />Intelligence Center of<br />Expertise tells you how";
    devDynamicContent.srFeed[0].copy.h2_sky= "Deloitte’s Artificial Intelligence Center of Expertise tells you how";
    
    devDynamicContent.srFeed[0].copy.cta = "More info";
    devDynamicContent.srFeed[0].color = {};
    devDynamicContent.srFeed[0].style = {};
    devDynamicContent.srFeed[0].style.h1Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.h2Color = "#FFFFFF";
    devDynamicContent.srFeed[0].style.ctaColor = "#FFFFFF";    

    return devDynamicContent;
}

module.exports = setData;
