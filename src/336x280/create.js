function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont(){
        add();
    }

    function add(){

        ___("h1")
            .text(dd.copy.h1_rect, {webfont:"open_sansbold", color:dd.style.h1Color,fontSize:18, width:225 })
            .style({css:"line-height:26px"})
            .position({top:75, left:20});

        ___("h2")
            .text(dd.copy.h2_rect, {webfont:"open_sansbold", color:dd.style.h2Color,fontSize:18, width:190})
            .style({css:"line-height:26px"})
            .position({top:75, left:20});

        ___("key_holder>key")
            .position({top:110, left:-50})
            .style({width:config.bannerWidth, height:config.bannerHeight, greensock:{
                    alpha:0
                }});
        // ___("key>key2")
        //     .image(fasset("key1.png"), {width:215, height:217, fit:true})
        //     .position({centerX:0, centerY:0})

        // ___("key>key1")
        //     .image(fasset("key1.png"), {width:215, height:216, fit:true})
        //     .position({centerX:0, centerY:0});

        ___("key>key1")
            .image(asset("key3.png"), {width:215, height:216, fit:true})
            .position({centerX:0, centerY:0});

        ___("deloitte_logo")
            .image(asset("deloitte-logo.png"), {width:150, height:30, fit:true})
            .position({left:20, top:20});


        ___("cta")
            .text(dd.copy.cta, {webfont:"open_sansbold", color:dd.style.ctaColor, fontSize:13, width:75})
            .position({right:46, bottom:26})
            .style({css:"padding:5px 15px; text-align: center; background-color: #0AA0DB;"});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTAG);
        }
    }
}

module.exports.setElements = setElements;