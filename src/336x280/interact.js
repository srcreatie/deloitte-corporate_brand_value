function animate(callback) {

    setAnimation();

    function setAnimation() {
        __("creative").style.opacity = 1;

        h1SplitText = new SplitText(h1, {type:"words"}),
        h2SplitText = new SplitText(h2, {type:"words"}),
        h1NumWords = h1SplitText.words.length;
        h2NumWords = h2SplitText.words.length;
        /*h1SplitText = new SplitText(h1, {type:"lines"}),
        h2SplitText = new SplitText(h2, {type:"lines"}),
        h1NumWords = h1SplitText.lines.length;
        h2NumWords = h2SplitText.lines.length;*/

        // TweenMax.set(h1, {transformPerspective:1000, perspective:100, transformStyle:"preserve-3d", autoAlpha:1});
        TweenMax.set(h1, {alpha:0});
        TweenMax.set(h2, {alpha:0});
        TweenMax.set(key, {scale:.5});

        TweenMax.set(key_holder, { perspective:750 })
        TweenMax.set(key, {transformStyle:"preserve-3d"})

        tl01 = new TimelineMax({});
        tlWords = new TimelineMax({});
        tlRO = new TimelineMax({/* paused : true */});

        tl01.from(deloitte_logo, 0.8, {alpha:0, x:"+=10", ease:Sine.easeOut, onComplete:function(){
                TweenMax.to(h1, 0, {alpha:1});
                playLine(h1SplitText, h1NumWords, tlWords)
            }
        }, "-=.5");

        // tl01.to(key, 1, { rotation:180, alpha:1, scale:.85, transformOrigin:"center center"}, "-=.5");
        tl01.to(key, 1, { alpha:1, scale:.85, transformOrigin:"center center"}, "-=.5");
        // tl01.to(key1, 2.6, {alpha:1, rotation:180, ease:Sine.easeInOut}, "keyTurn");
        // tl01.to(key2, 3, {alpha:1, rotation:-180, ease:Sine.easeInOut}, "keyTurn+=0.2");
        // tl01.to(key1, 2, {rotation:-180, ease:Sine.easeInOut, alpha:0, scale:0.5, transformOrogin:"center center"}, "keyTurn+=3");
        // tl01.to(key2, 2, {rotation:180, ease:Sine.easeInOut}, "keyTurn+=3.2");
        // tl01.to(this, 0, {onComplete:function(){
        //     TweenMax.to(h1, 0, {alpha:1});
        //     playLine(h1SplitText, h1NumWords, tlWords)
        //     }
        // }, "-=.5");
        //tl01.to(key, 1.8, {scale:0.8, transformOrigin:"center center"})
        tl01.to(this, 1.8, {});

        tl01.to(this, 1,{ onComplete:function(){
            removeLine(h1SplitText, h1NumWords, tlWords)
        }});

        // tl01.to(key, 1, {x:170, y:-125,rotation:360, scale:.85, alpha:1, ease:Sine.easeInOut});
        tl01.to(key, 1, {x:170, y:-125, scale:.85, alpha:1, ease:Sine.easeInOut});
        
        tl01.to(key, 0.5, { rotationY:20, skewX:"-=3", ease:Power2.easeInOut, yoyo:true, repeat:1 });
        tl01.to(this, 1, {})
        tl01.to(key, 0.5, { rotationX:20, skewY:"-=5", ease:Power2.easeInOut, yoyo:true, repeat:1 });

        tl01.to(this, 2,{ onStart:function(){
            TweenMax.to(h2, 0, {alpha:1});
            playLine(h2SplitText, h2NumWords, tlWords)
        }}, "-=3.5");

        tl01.to(this, .5, {onComplete: function () {
            tlRO.from(cta, 0.4, {yoyo: true, scale:1.1, transformOrigin:"50% 50%", ease:Sine.easeInOut}, "btnRollOver");
            tlRO.from(cta, 0.4, {yoyo: true, css:{backgroundColor:"#4FBCEA"}}, "btnRollOver");
        }})

        tl01.to(this, 0, {onComplete:rollOver})

        function playLine(text, amount, tl) {

            for(var i = 0; i < amount; i++){
                tl.from(text.words[i], .4, {x:"+=10", alpha:0, ease:Quad.easeOut  }, "-=0.2");
            }
        }

        function removeLine(text, amount, tl) {
            for(var i = 0; i < amount; i++){
                tl.to(text.words[i], .2, {alpha:0, x: "-=150", alpha:0, ease:Quad.easeIn  }, "-=0.2");
            }
        }

        // Splttext function mandatory for animation
        function randomNumber(min, max){
            return Math.floor(Math.random() * (1 + max - min) + min);
        }

        /** Rollover functions */
        function rollOver(element){
            banner.onmouseover= function(){
                tlRO.reverse();
            };

            banner.onmouseout= function(){
                tlRO.play();
            };
        }


        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }
    }

    if (callback) {
        callback();
    }
}

module.exports.animate = animate;