function setElements(callback) {

    config = {};
    config.bannerWidth = 120;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont(){
        add()
    }

    function add(){

        ___("h1")
            .text(dd.copy.h1_sky, {webfont:"open_sansbold", color:dd.style.h1Color,fontSize:14, width:100 })
            .style({css:"line-height:22px"})
            .position({top:85, left:10});

        ___("h2")
            .text(dd.copy.h2_sky, {webfont:"open_sansbold", color:dd.style.h2Color,fontSize:14, width:100})
            .style({css:"line-height:22px"})
            .position({top:85, left:10});

        // ___("key")
        //     .image(fasset("key1.png"), {width:215, height:217, fit:true})
        //     .position({left:-110, centerY:80, greensock:{alpha:0}})

        ___("key_holder>key")
            .image(asset("key3.png"), {width:215, height:217, fit:true})
            .position({left:-110, centerY:80, greensock:{alpha:0}})


        ___("deloitte_logo")
            .image(asset("deloitte-logo.png"), {width:100, height:25, fit:true})
            .position({left:10, top:30});

        ___("cta")
            .text(dd.copy.cta, {webfont:"open_sansbold", color:dd.style.ctaColor, fontSize:11, width:70})
            .position({centerX:-15, bottom:60})
            .style({css:"padding:5px 15px; text-align: center; background-color: #0AA0DB;"});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTAG);
        }
    }
}

module.exports.setElements = setElements;