function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont(){
        add();
    }

    function add(){

        ___("h1")
            .text(dd.copy.h1_rect, {webfont:"open_sansbold", color:dd.style.h1Color,fontSize:17, width:250 })
            .style({css:"line-height:24px"})
            .position({top:10, left:175});

        ___("h2")
            .text(dd.copy.h2_leader, {webfont:"open_sansbold", color:dd.style.h2Color,fontSize:17, width:250})
            .style({css:"line-height:24px"})
            .position({top:10, left:175});

        // ___("key")
        //     .image(asset("key1.png"), {width:215, height:217, fit:true})
        //     .position({left:375, centerY:42, greensock:{alpha:0}})

        ___("key_holder>key")
            .image(asset("key3.png"), {width:215, height:217, fit:true})
            .position({left:375, centerY:42, greensock:{alpha:0}})

        ___("deloitte_logo")
            .image(asset("deloitte-logo.png"), {width:120, height:25, fit:true})
            .position({left:25, top:30});

        ___("cta")
            .text(dd.copy.cta, {webfont:"open_sansbold", color:dd.style.ctaColor, fontSize:11, width:90})
            .position({right:46, centerY:-7})
            .style({css:"padding:5px 15px; text-align: center; background-color: #0AA0DB;"});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTAG);
        }
    }
}

module.exports.setElements = setElements;