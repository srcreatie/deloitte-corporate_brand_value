function setElements(callback) {

    config = {};
    config.bannerWidth = 320;
    config.bannerHeight = 100;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont(){
        add();
    }

    function add(){

        ___("h1")
            .text(dd.copy.h1_rect, {webfont:"open_sansbold", color:dd.style.h1Color,fontSize:12, width:250 })
            .style({css:"line-height:14px"})
            .position({top:45, left:10});

        ___("h2")
            .text(dd.copy.h2_leader, {webfont:"open_sansbold", color:dd.style.h2Color,fontSize:12, width:250})
            .style({css:"line-height:14px"})
            .position({top:45, left:10});

        // ___("key")
        //     .image(assets("key1.png"), {width:215, height:217, fit:true})
        //     .position({left:130, centerY:0, greensock:{alpha:0}})

        ___("key")
            .image(asset("key3.png"), {width:215, height:217, fit:true})
            .position({left:130, centerY:0, greensock:{alpha:0}})

        ___("deloitte_logo")
            .image(asset("deloitte-logo.png"), {width:100, height:20, fit:true})
            .position({left:10, top:10});

        ___("cta")
            .text(dd.copy.cta, {webfont:"open_sansbold", color:dd.style.ctaColor, fontSize:11, width:75})
            .position({right:42, bottom:23})
            .style({css:"padding:5px 15px; text-align: center; background-color: #0AA0DB;"});

        sr.loading.done(callback);
    }
}


function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);

    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTAG);
        }
    }
}

module.exports.setElements = setElements;